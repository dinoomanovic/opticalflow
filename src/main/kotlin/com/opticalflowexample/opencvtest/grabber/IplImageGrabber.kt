package com.opticalflowexample.opencvtest.grabber

import org.bytedeco.javacpp.opencv_core
import org.bytedeco.javacpp.opencv_core.cvClearMemStorage
import org.bytedeco.javacpp.opencv_core.cvReleaseMemStorage
import org.bytedeco.javacv.CanvasFrame
import org.bytedeco.javacv.OpenCVFrameConverter
import org.bytedeco.javacv.OpenCVFrameGrabber

class IplImageGrabber(private val title: String = "IplImageGrabber") {

    fun grab(deviceNumber: Int = 0, function: (opencv_core.IplImage) -> opencv_core.IplImage) {
        val grabber = OpenCVFrameGrabber(deviceNumber)
        val converter = OpenCVFrameConverter.ToIplImage()
        var canvasFrame: CanvasFrame? = null
        var memoryStorage: opencv_core.CvMemStorage? = null

        try {
            grabber.start()
            var frame = converter.convert(grabber.grab())
                ?: throw IllegalStateException("Failed to grab initial frame.")

            canvasFrame = CanvasFrame(title)
            canvasFrame.isResizable = false
            canvasFrame.setCanvasSize(frame.width(), frame.height())

            memoryStorage = opencv_core.CvMemStorage.create()
            var image: opencv_core.IplImage?

            while (canvasFrame.isVisible) {
                frame = converter.convert(grabber.grab()) ?: break
                cvClearMemStorage(memoryStorage)
                image = function(frame)
                canvasFrame.showImage(converter.convert(image))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            grabber.stop()
            canvasFrame?.dispose()
            memoryStorage?.let(::cvReleaseMemStorage)
        }
    }
}
