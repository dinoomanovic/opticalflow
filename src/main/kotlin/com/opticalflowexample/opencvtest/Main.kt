package com.opticalflowexample.opencvtest

import boofcv.factory.flow.FactoryDenseOpticalFlow
import boofcv.gui.PanelGridPanel
import boofcv.gui.feature.VisualizeOpticalFlow
import boofcv.gui.image.ShowImages
import boofcv.io.image.ConvertBufferedImage
import boofcv.io.image.UtilImageIO.loadImage
import boofcv.struct.flow.ImageFlow
import boofcv.struct.image.GrayF32
import com.opticalflowexample.opencvtest.grabber.IplImageGrabber
import com.opticalflowexample.opencvtest.grabber.MatGrabber
import org.bytedeco.javacpp.opencv_core
import org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U
import org.bytedeco.javacpp.opencv_core.Mat
import org.bytedeco.javacpp.opencv_imgproc
import java.awt.Panel
import java.awt.image.BufferedImage
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.WindowConstants

fun cannyWithIplImageGrabber() {
    val grabber = IplImageGrabber()

    val threshold: Double = 20.0
    val apertureSize: Int = 3

    grabber.grab {
        val image: opencv_core.IplImage = opencv_core.IplImage.create(
            it.width(),
            it.height(),
            opencv_core.IPL_DEPTH_8U,
            1
        ) // create new image for gray scale converting
        opencv_imgproc.cvCvtColor(
            it,
            image,
            opencv_imgproc.CV_BGR2GRAY
        ) // convert image to gray scale
        opencv_imgproc.cvCanny(
            image,
            image,
            threshold,
            (threshold * 9),
            apertureSize
        ) // apply canny
        image // pass image for display
    }
}

fun cannyWithMatGrabber() {
    val grabber = MatGrabber()

    val threshold: Double = 20.0
    val apertureSize: Int = 3

    grabber.grab {
        val image =
            Mat(
                it.size().width(),
                it.size().height(),
                IPL_DEPTH_8U,
                1
            ) // create new image for gray scale converting
        opencv_imgproc.cvtColor(
            it,
            image,
            opencv_imgproc.CV_BGR2GRAY
        ) // convert image to gray scale
        opencv_imgproc.Canny(
            image,
            image,
            threshold,
            (threshold * 9),
            apertureSize,
            true
        ) // apply canny
        image // pass image for display
    }
}

fun denseOpticalFlowTest() {
    try {
        // Load images
        val imgNormal = loadImage("images/img0.jpg") // Normal brain MRI
        val imgTumor = loadImage("images/img1.jpg")  // Tumor brain MRI

        // Convert images to GrayF32 for BoofCV
        val grayNormal = ConvertBufferedImage.convertFrom(imgNormal, null as GrayF32?)
        val grayTumor = ConvertBufferedImage.convertFrom(imgTumor, null as GrayF32?)

        // Compute dense optical flow
        val flow = ImageFlow(grayNormal.width, grayNormal.height)
        val denseFlow = FactoryDenseOpticalFlow.region(null, GrayF32::class.java)
        denseFlow.process(grayNormal, grayTumor, flow)

        // Create visualizations of the optical flow
        val visualizedNormal = BufferedImage(grayNormal.width, grayNormal.height, BufferedImage.TYPE_INT_RGB)
        visualizeThresholdedFlow(flow, visualizedNormal, threshold = 1.5f)

        val visualizedTumor = BufferedImage(grayTumor.width, grayTumor.height, BufferedImage.TYPE_INT_RGB)
        visualizeThresholdedFlow(flow, visualizedTumor, threshold = 1.5f)

        // Prepare the panel for 4 images: Normal, Tumor, Flow on Normal, Flow on Tumor
        val gui = PanelGridPanel(1, 3)

        // Add the actual images
        gui.add(javax.swing.JLabel(javax.swing.ImageIcon(imgNormal)))        // Normal brain MRI
        gui.add(javax.swing.JLabel(javax.swing.ImageIcon(imgTumor)))         // Tumor brain MRI
        gui.add(javax.swing.JLabel(javax.swing.ImageIcon(visualizedNormal))) // Flow on Normal MRI
        // gui.add(javax.swing.JLabel(javax.swing.ImageIcon(visualizedTumor)))  // Flow on Tumor MRI

        // Display the images in a window
        ShowImages.showWindow(gui, "Dense Optical Flow: Normal vs Tumor with Flow Visualizations", true)

    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun openCVTest() {
    try {
        // Load images
        val imgNormal = loadImage("images/img0.jpg") // Normal brain MRI
        val imgTumor = loadImage("images/img1.jpg")  // Tumor brain MRI

        // Convert images to GrayF32
        val grayNormal = ConvertBufferedImage.convertFrom(imgNormal, null as GrayF32?)
        val grayTumor = ConvertBufferedImage.convertFrom(imgTumor, null as GrayF32?)

        // Compute dense optical flow
        val flow = ImageFlow(grayNormal.width, grayNormal.height)
        val denseFlow = FactoryDenseOpticalFlow.region(null, GrayF32::class.java)
        denseFlow.process(grayNormal, grayTumor, flow)

        // Create a BufferedImage for highlighting the tumor area
        val highlightedTumor = BufferedImage(grayTumor.width, grayTumor.height, BufferedImage.TYPE_INT_RGB)

// Calculate dynamic thresholds for magnitude and intensity
        val flowMagnitudeThreshold = 0.1 * flow.width  // Example: 10% of the image width
        val intensityThreshold = grayTumor.data.sum() / grayTumor.data.size + 50  // Mean intensity + buffer

// Iterate through the flow and grayTumor to highlight areas with significant changes
        for (y in 0 until flow.height) {
            for (x in 0 until flow.width) {
                val vector = flow.get(x, y)

                // Calculate normalized magnitude
                val magnitude = Math.sqrt((vector.x * vector.x + vector.y * vector.y).toDouble())

                // Get intensity value
                val intensity = grayTumor.get(x, y)

                // Highlight areas with both significant flow and intensity
                if (magnitude > flowMagnitudeThreshold && intensity > intensityThreshold) {
                    highlightedTumor.setRGB(x, y, java.awt.Color.WHITE.rgb)  // Tumor area in white
                } else {
                    highlightedTumor.setRGB(x, y, java.awt.Color.BLACK.rgb)  // Other areas in black
                }
            }
        }
        // Display: Normal, Tumor, and Highlighted Tumor Changes
        val gui = PanelGridPanel(1, 3)
        gui.add(javax.swing.JLabel(javax.swing.ImageIcon(imgNormal)))          // Normal brain MRI
        gui.add(javax.swing.JLabel(javax.swing.ImageIcon(imgTumor)))           // Tumor brain MRI
        gui.add(javax.swing.JLabel(javax.swing.ImageIcon(highlightedTumor)))   // Highlighted tumor regions

        ShowImages.showWindow(gui, "OpenCV: Normal vs Tumor with Highlighted Tumor", true)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

/**
 * Thresholds the optical flow to visualize only significant changes.
 *
 * @param flow The computed optical flow.
 * @param output The BufferedImage to store the visualization.
 * @param threshold The magnitude threshold for filtering low-motion regions.
 */
fun visualizeThresholdedFlow(flow: ImageFlow, output: BufferedImage, threshold: Float) {
    val width = flow.width
    val height = flow.height

    for (y in 0 until height) {
        for (x in 0 until width) {
            val v = flow.get(x, y)
            if (!v.isValid || Math.sqrt((v.x * v.x + v.y * v.y).toDouble()) < threshold) {
                // Set to black if below threshold or invalid
                output.setRGB(x, y, 0x000000)
            } else {
                // Set to a colorized representation of the flow
                val hue = Math.atan2(v.y.toDouble(), v.x.toDouble()).toFloat() / (2 * Math.PI.toFloat()) + 0.5f
                val saturation = 1f
                val brightness = 1f
                val rgb = java.awt.Color.HSBtoRGB(hue, saturation, brightness)
                output.setRGB(x, y, rgb)
            }
        }
    }
}


fun main() {
    val frame = JFrame("Optical Flow Examples")
    val panel = Panel()

    // Updated button titles
    val button1 = JButton("Normal vs Tumor")
    val button4 = JButton("Tumor Highlighted")

    frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE

    panel.add(button1)
    panel.add(button4)
    frame.add(panel)

    button1.addActionListener {
        denseOpticalFlowTest()
    }

    button4.addActionListener {
        openCVTest()
    }

    frame.pack()
    frame.isVisible = true
}